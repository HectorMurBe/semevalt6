import os
class SemevalLoader():
    @staticmethod
    def loadDataT1(pathToTrainData,pathToDevData):
        """
        Method to load data for task 1
        input: path to deft files
        output: [TrainDefinitions],[TrainLabels],[DevDefinitions],[DevLabels]
        """
        trainDefinitions,trainLabels = SemevalLoader._loadData(pathToTrainData)
        devDefinitions,devLabels = SemevalLoader._loadData(pathToDevData)
        return trainDefinitions,trainLabels,devDefinitions,devLabels
    @staticmethod
    def _loadData(pathToData):
        """
        method to load task 1 data from a single folder
        """
        if pathToData[-1]!='/':
            pathToData=pathToData+'/'
        definitions=[]
        labels=[]
        for fileName in os.listdir(pathToData):
            fileDefinitions=[]
            fileLabels=[]
            with open(pathToData+fileName,'r') as tempFile:
                for line in tempFile.readlines():
                    splitedLine=line.replace('"',"").replace("\n","").split("\t")
                    fileDefinitions.append(splitedLine[0])
                    fileLabels.append(splitedLine[1])
            definitions=definitions+fileDefinitions
            labels=labels+fileLabels
        return definitions,labels